$(function(){
	$("#wizard").steps({
        headerTag: "h4",
        bodyTag: "section",
        transitionEffect: "fade",
        enableAllSteps: true,
        transitionEffectSpeed: 150,
        labels: {
            next: "Вперед",
            previous: "Назад"
        },
        onStepChanging: function (event, currentIndex, newIndex) { 
		
			$('.steps ul').addClass('step-'+ (newIndex+1));
			$('.steps ul').removeClass('step-'+ (currentIndex+1));
			
			if(newIndex==3){				
				$('.form-holder').each(function(i,e){					
					if($(e).find('input')[0]) {	
						switch ($(e).find('input')[0].type){
							case "text":
								var textValue = $(e).find('input').val();
								$($('tbody tr #tdvalue')[i]).html(textValue);								
							break;
							case "radio":
								var radioValue = "";
								$(e).find("input[type='radio']:checked").each(function(i,e){
									radioValue = e.parentElement.innerText;
								});
								$($('tbody tr #tdvalue')[i]).html(radioValue);				 
							break;
							case "checkbox":
								var checkBoxValues = "";
								$(e).find('input:checked').each(function(i,e){
									checkBoxValues += $(e).parent()[0].innerText + "; ";
								})
								$($('tbody tr #tdvalue')[i]).html(checkBoxValues);			
							break;							
						}			
					} else if($(e).find('select option:selected')[0]) {
						var selectValues = $(e).find('select option:selected').html();
						$($('tbody tr #tdvalue')[i]).html(selectValues);
					}
				})				
			}
				
				return true; 
        }
    });
    // Custom Button Jquery Steps
    $('.forward').click(function(){
    	$("#wizard").steps('next');
    })
    $('.backward').click(function(){
        $("#wizard").steps('previous');
    })
    // Grid 
    $('.grid .grid-item').click(function(){
        $('.grid .grid-item').removeClass('active');
        $(this).addClass('active');
    })
    // Click to see password 
    $('.password i').click(function(){
        if ( $('.password input').attr('type') === 'password' ) {
            $(this).next().attr('type', 'text');
        } else {
            $('.password input').attr('type', 'password');
        }
    }) 
    // Date Picker
    var dp1 = $('#dp1').datepicker().data('datepicker');
    dp1.selectDate( new Date("01-01-1970"));
	var dp2 = $('#dp2').datepicker().data('datepicker');
    dp2.selectDate( new Date('01-01-1992'));
})
